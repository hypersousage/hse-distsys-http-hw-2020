import os
import asyncio
import time
import uuid

from aiohttp import web


class Server():
    def __init__(self):
        self._storage = dict()
        self._article_properties = ['title', 'body']
        self._article_internal_properties = ['creation_ts', 'last_modified_ts']

    def _get_timestamp(self):
        return int(time.time()) * 1000

    async def add_article(self, request):
        try:
            data = await request.json()
            title = data['title']
            body = data['body']
            creation_ts = self._get_timestamp()
            last_update_ts = creation_ts
            article_id = uuid.uuid4().hex
            if article_id in self._storage:
                return web.json_response({
                    'result': False,
                    'error': 'Article with generated id already exist. Please try again.'
                })
            self._storage[article_id] = {
                'title': title,
                'body': body,
                'creation_ts': creation_ts,
                'last_update_ts': last_update_ts
            }
            return web.json_response({
                'result': True,
                'id': article_id
            })
        except Exception as e:
            return web.json_response({
                'result': False,
                'error': str(e)
            })

    async def delete_article(self, request):
        try:
            article_id = request.match_info['id']
            self._storage.pop(article_id, None)
            return web.json_response({
                'result': True
            })
        except Exception as e:
            return web.json_response({
                'result': False,
                'error': str(e)
            })

    async def delete_all_articles(self, request):
        try:
            self._storage = {}
            return web.json_response({
                'result': True
            })
        except Exception as e:
            return web.json_response({
                'result': False,
                'error': str(e)
            })

    async def get_articles_info(self, request):
        try:
            response = {'result': True, 'articles': self._storage}
            return web.json_response(response)
        except Exception as e:
            return web.json_response({
                'result': False,
                'error': str(e)
            })

    async def get_article_info(self, request):
        try:
            article_id = request.match_info['id']
            article_info = self._storage.get(article_id)
            if not article_info:
                return web.json_response({
                    'result': False,
                    'error': f'There is no article with id {article_id}'
                }, status=404)
            else:
                response = {'result': True}
                response.update(article_info)
                return web.json_response(response)
        except Exception as e:
            return web.json_response({
                'result': False,
                'error': str(e)
            })

    async def replace_article(self, request):
        try:
            article_id = request.match_info['id']
            if not article_id in self._storage:
                return web.json_response({
                    'result': False,
                    'error': 'Article is not found.'
                }, status=404)
            data = await request.json()
            if not all([key in data for key in self._article_properties]):
                return web.json_response({
                    'result': False,
                    'error': 'Not enough parameters to create article.'
                }, status=400)
            creation_ts = self._get_timestamp()
            last_update_ts = creation_ts
            self._storage[article_id] = {
                'title': data['title'],
                'body': data['body'],
                'creation_ts': creation_ts,
                'last_update_ts': last_update_ts
            }
            return web.json_response({
                'result': True
            })
        except Exception as e:
            return web.json_response({
                'result': False,
                'error': str(e)
            })

    async def modify_article(self, request):
        try:
            article_id = request.match_info['id']
            if not article_id in self._storage:
                return web.json_response({
                    'result': False,
                    'error': 'Article is not found.'
                }, status=404)
            data = await request.json()
            for prop in self._article_properties:
                if prop in data:
                    cur_ts = self._get_timestamp()
                    self._storage[article_id][prop] = data[prop]
                    self._storage[article_id]['last_update_ts'] = cur_ts
            return web.json_response({
                'result': True
            })
        except Exception as e:
            return web.json_response({
                'result': False,
                'error': str(e)
            })


def create_app(handler):
    app = web.Application()
    app.router.add_get('/articles', handler.get_articles_info)
    app.router.add_get('/articles/{id}', handler.get_article_info)
    app.router.add_post('/articles', handler.add_article)
    app.router.add_put('/articles/{id}', handler.replace_article)
    app.router.add_patch('/articles/{id}', handler.modify_article)
    app.router.add_delete('/articles/{id}', handler.delete_article)
    app.router.add_delete('/articles', handler.delete_all_articles)


    return app

if __name__ == '__main__':
    server = Server()
    app = create_app(server)
    web.run_app(app, port=int(os.environ.get('HSE_HTTP_SERVER_PORT', 80)))