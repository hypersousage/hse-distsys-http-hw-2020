import os
import requests

SERVER_HOST = os.environ.get('HSE_HTTP_TESTS_SERVER_HOST', 'server')
SERVER_PORT = int(os.environ.get('HSE_HTTP_SERVER_PORT', 80))
URL = 'http://' + SERVER_HOST
if SERVER_PORT != 80:
    URL += ':{}'.format(SERVER_PORT)

def test_get_articles():
    delete_response = requests.delete(f'{URL}/articles')
    delete_response_json = delete_response .json()
    assert delete_response_json['result'] == True
    assert delete_response.status_code == 200

    data = {
        'title': 'Test title',
        'body': 'Test body',
    }
    response = requests.post(f'{URL}/articles', json=data)
    response_json = response.json()
    assert response_json['result'] == True
    assert response.status_code == 200
    created_id = response_json['id']

    get_response = requests.get(f'{URL}/articles')
    get_response_json = get_response.json()
    assert get_response_json['result'] == True
    assert get_response.status_code == 200
    assert len(get_response_json['articles']) == 1
    assert get_response_json['articles'][created_id]['body'] == 'Test body'
    assert get_response_json['articles'][created_id]['title'] == 'Test title'
