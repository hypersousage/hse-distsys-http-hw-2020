import os
import requests

SERVER_HOST = os.environ.get('HSE_HTTP_TESTS_SERVER_HOST', 'server')
SERVER_PORT = int(os.environ.get('HSE_HTTP_SERVER_PORT', 80))
URL = 'http://' + SERVER_HOST
if SERVER_PORT != 80:
    URL += ':{}'.format(SERVER_PORT)

def test_add_article():
    data = {
        'title': 'Test title',
        'body': 'Test body',
    }
    response = requests.post(f'{URL}/articles', json=data)
    response_json = response.json()
    assert response_json['result'] == True
    assert response.status_code == 200
