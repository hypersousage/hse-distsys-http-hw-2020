import os
import requests

SERVER_HOST = os.environ.get('HSE_HTTP_TESTS_SERVER_HOST', 'server')
SERVER_PORT = int(os.environ.get('HSE_HTTP_SERVER_PORT', 80))
URL = 'http://' + SERVER_HOST
if SERVER_PORT != 80:
    URL += ':{}'.format(SERVER_PORT)

def test_replace_article():
    data = {
        'title': 'Test title',
        'body': 'Test body',
    }
    response = requests.post(f'{URL}/articles', json=data)
    response_json = response.json()
    assert response_json['result'] == True
    assert response.status_code == 200
    created_id = response_json['id']

    new_data = {
        'title': 'New title'
    }
    failed_response = requests.put(f'{URL}/articles/{created_id}', json=new_data)
    failed_response_json = failed_response.json()
    assert failed_response_json['result'] == False
    assert failed_response.status_code == 400

    new_data['body'] = 'New body'
    successful_resonse = requests.put(f'{URL}/articles/{created_id}', json=new_data)
    successful_resonse_json = successful_resonse.json()
    assert successful_resonse_json['result'] == True
    assert successful_resonse.status_code == 200

    get_response = requests.get(f'{URL}/articles/{created_id}')
    get_response_json = get_response.json()
    assert get_response_json['result'] == True
    assert get_response.status_code == 200
    assert get_response_json['body'] == 'New body'
    assert get_response_json['title'] == 'New title'
