**Article storage API endpoints**
=================================

**New article**
----
  `POST /articles`

  Create a new article.
  
**Example Request Json:**

```
{
    "title": "Test title",  # Required
    "body": "Test body",    # Required
}
```

**Success Response:**
  
```
{ 
    "id": "0c07652bc708488599c33e2699c47286",
    "result": true
}
```

**Get articles info**
----
  `GET /articles`

  Get info of all stored articles.

**Success Response:**
  
```
{ 
    "id": "0c07652bc708488599c33e2699c47286",
    "result": true,
    "articles": {
        "0c07652bc708488599c33e2699c47286": {
            "title": "Title",
            "body": "Body",
            "creation_ts": 1607868390334,    # Timestamp in ms
            "last_update_ts": 1607868390334  # Timestamp in ms
        }
    }
}
```

**Get article info**
----
  `GET /articles/{id}`

  Get info of an article with specified id.

**Success Response:**
  
```
{
    "result": true,
    "title": "Title",
    "body": "Body",
    "creation_ts": 1607868390334,    # Timestamp in ms
    "last_update_ts": 1607868390334  # Timestamp in ms
}
```

**Replace article**
----
  `PUT /articles/{id}`

  Replace an existing article.
  Note: creation_ts will not be saved from existing article
  
**Example Request Json:**

```
{
    "title": "Test title",  # Required
    "body": "Test body",    # Required
}
```

**Success Response:**
  
```
{
    "result": true
}
```

**Modify article**
----
  `PATCH /articles/{id}`

  Modify an existing article.
  
**Example Request Json:**

```
{
    "title": "Test title",  # Optional
    "body": "Test body",    # Optional
}
```

**Success Response:**
  
```
{
    "result": true
}
```

**Delete articles**
----
  `DELETE /articles`

  Delete all stored articles.

**Success Response:**
  
```
{
    "result": true
}
```

**Delete article**
----
  `DELETE /articles/{id}`

  Delete an article with specified id.

**Success Response:**
  
```
{
    "result": true
}
```